package main

import (
	ps "github.com/mitchellh/go-ps"
	"fmt"
	"bufio"
	"strings"
	"os"
	"os/exec"
	"runtime"
)

func javaInfo() (infoItem, error) {
	javaOutput, err := exec.Command("java", "-version").CombinedOutput()
	var lines []string
	if err == nil {
		lines = strings.Split(string(javaOutput), "\n")
	} else {
		lines = []string{err.Error(), "there was an error requesting the java version, is it set up correctly?"}
	}
	for index, line := range(lines) {
		if strings.Trim(line, " \t") == "" {
			lines[len(lines)-1], lines[index] = lines[index], lines[len(lines)-1]
			lines = lines[:len(lines)-1]
		}
	}
	return infoItem{"JAVA", lines}, nil
}

func hostInfo() (infoItem, error) {
	var lines []string
	var path  string

	/*
	if runtime.GOOS == "linux" {
		path = "/etc/hosts"
	} else if runtime.GOOS == "darwin" {
		path = "/private/etc/hosts"
	}
	*/

	switch runtime.GOOS {
		case "linux":
			path = "/etc/hosts"
		case "darwin":
			path = "/private/etc/hosts"
		case "windows":
			path = "%%SystemRoot%%/System32/drivers/etc/hosts"
		case "default":
			Logger.error("platform error")
	}

	file, err := os.Open(path)
	defer file.Close()
	if err != nil { return infoItem{}, err }
	scanner := bufio.NewScanner(file)
	var entry string
	for scanner.Scan() {
		entry = ""
		text := strings.Split(scanner.Text(), "")
		for _, char := range(text) {
			if char != "#" {
				entry += char
			} else {
				break
			}
		}

		for {
			entry = strings.Replace(entry, "\t", " ", -1)
			entry = strings.Replace(entry, "  ", " ", -1)
			if !strings.Contains(entry, "  ") && !strings.Contains(entry, "\t") { break }
		}

		if entry != "" && entry != " " {
			lines = append(lines, entry)
		}
	}
	return infoItem{"HOST", lines}, nil
}

func kernelInfo() (infoItem, error) {
	var output string
	if runtime.GOOS == "darwin" { return infoItem{"VERSION", []string{"this info is not yet available for your platform"}}, nil }
	file, err := os.Open("/proc/version")
	defer file.Close()
	if err != nil { return infoItem{}, err }
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		output += scanner.Text() + " "
	}

	return infoItem{"VERSION", []string{output}}, nil
}

func processInfo() (infoItem, error) {
	var lines []string
	processes, err := ps.Processes()
	if err != nil { return infoItem{}, err }
	for index, process := range(processes) {
		lines = append(lines, fmt.Sprintf("#%v w/ID %v %v", index, process.Pid(), process.Executable()))
	}

	return infoItem{"PROCESS", lines}, nil
}