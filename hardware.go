package main

import (
	"github.com/jaypipes/ghw"
	"fmt"
)

func cpuInfo() (infoItem, error) {
	var lines []string
	cpu, err := ghw.CPU()
	if err != nil { return infoItem{}, err }
	virtual    := cpu.TotalThreads
	physical   := cpu.TotalCores
	processors := cpu.Processors
	count      := len(processors)

	lines = append(lines, fmt.Sprintf("%v processor packages, %v cores, %v threads", count, physical, virtual))

	for index, processor := range(processors) {
		lines = append(lines, fmt.Sprintf("package #%v:", index))
		lines = append(lines, fmt.Sprintf("| vendor: %v", processor.Vendor))
		lines = append(lines, fmt.Sprintf("| model: %v", processor.Model))
		lines = append(lines, fmt.Sprintf("| cores: %v", processor.NumCores))
		lines = append(lines, fmt.Sprintf("| threads: %v", processor.NumThreads))                                    
	}

	return infoItem{"", lines}, nil
}

func gpuInfo() (infoItem, error) {
	var lines []string
	gpu, err := ghw.GPU()
	if err != nil { return infoItem{}, err }
	graphicsCards := gpu.GraphicsCards
	count := len(graphicsCards)

	lines = append(lines, fmt.Sprintf("%v graphics devices", count))

	for index, graphicsCard := range(graphicsCards) {
		lines = append(lines, fmt.Sprintf("device #%v:", index))
		lines = append(lines, fmt.Sprintf("| vendor: %v", graphicsCard.DeviceInfo.Vendor.Name))
		lines = append(lines, fmt.Sprintf("| model: %v", graphicsCard.DeviceInfo.Product.Name))
		lines = append(lines, fmt.Sprintf("| address: %v", graphicsCard.DeviceInfo.Address))
	}

	return infoItem{"", lines}, nil
}

func memInfo() (infoItem, error) {
	memory, err := ghw.Memory()
	if err != nil { return infoItem{}, err }
	return infoItem{
		"",
		[]string{fmt.Sprintf("%vGB physical memory, %vGB usable", memory.TotalPhysicalBytes / 1000000000, memory.TotalUsableBytes / 1000000000)},
	}, nil
}